import media
import fresh_tomatoes

toy_story = media.Movie("Toy Story", "A story of a boy and his toys that come to life", "http://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg", "https://www.youtube.com/watch?v=vwyZH85NQC4")
#print(toy_story.storyline)

avatar = media.Movie("Avatar", "A marine on an alien planet", "http://upload.wikimedia.org/wikipedia/id/b/b0/Avatar-Teaser-Poster.jpg", "http://www.youtube.com/watch?v=-9ceBgWV8io")
#avatar.show_trailer()
#print(avatar.storyline)


ex_machina = media.Movie("Ex Machina", "Robots man.", "http://www.joblo.com/posters/images/full/ex-machina-poster.jpg", "https://youtu.be/EoQuVnKhxaM")
#ex_machina.show_trailer()

sinister = media.Movie("Sinister", "Bad things happen.", "http://www.impawards.com/2012/posters/sinister.jpg", "https://youtu.be/_kbQAJR9YWQ")

book_of_life = media.Movie("Book of Life", "Bright colors.", "http://www.impawards.com/2014/posters/book_of_life.jpg", "https://youtu.be/_i69CJc1BgE")

smokin_aces = media.Movie("Smokin' Aces", "Shoot em' up.", "https://image.tmdb.org/t/p/original/nrdnN8WqvyOt8Bnl3hgWcZD6ZyM.jpg", "https://youtu.be/ohhxbsp8Mss")

movies = [toy_story, avatar, ex_machina, sinister, book_of_life, smokin_aces]
fresh_tomatoes.open_movies_page(movies)
