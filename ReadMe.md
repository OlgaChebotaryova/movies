# Movies Project

Project handles a logic to create web page and fill it with movies
details prepopulated at _entertainment_center.py_ file.

## To run

Start _entertainment_center.py_
No special requirements needed.

## Output

Resulting html file will be generated at **fresh_tomatoes.html**