#!/usr/bin/env python
# -*- coding: utf-8 -*-
import webbrowser


class Movie():
    """
    Class to represent Movie object
    Attributes:
        title (str): movie title
        storyline (str): movie storyline
        poster_image_url (str): url to the movie image
        trailer_youtube_url (str): url of the trailer at Youtube

    Methods:
        show_trailer(): opens web browser with movie trailer
    """
    def __init__(self, title, storyline, poster_image, youtube_trailer):
        """

        :param title: Movie title
        :type title: string
        :param storyline: Short story description
        :type storyline: string
        :param poster_image: Poster image
        :type poster_image: string
        :param youtube_trailer: Youtube trailer
        :type youtube_trailer: string
        """
        self.title = title
        self.storyline = storyline
        self.poster_image_url = poster_image
        self.trailer_youtube_url = youtube_trailer

    # Show trailer in web browser
    def show_trailer(self):
        webbrowser.open(self.trailer_youtube_url)
